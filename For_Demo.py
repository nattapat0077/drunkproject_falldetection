import cv2
import time
import numpy as np
import csv
import os

protoFile = "pose/pose_deploy_linevec_faster_4_stages.prototxt"
weightsFile = "pose/pose_iter_160000.caffemodel"
nPoints = 15
POSE_HEAD = [[0,1]]
POSE_SHOULDER = [[2,5]]
POSE_BODY = [[1,14]]

inWidth = 368
inHeight = 368  
threshold = 0.1

input_source = "Data/Full.mp4"
# input_source = "sample_video.mp4"
cap = cv2.VideoCapture(input_source)
# cap = cv2.VideoCapture(0)
hasFrame, frame = cap.read()

net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)

TIME_FRAME = [1,1,1]

HEAD_SGX = [0,0]
HEAD_SGY = [0,0]
HEAD_SPEED_GX = [0,0]
HEAD_SPEED_GY = [0,0]

SHOULDER_SGX = [0,0]
SHOULDER_SGY = [0,0]
SHOULDER_SPEED_GX = [0,0]
SHOULDER_SPEED_GY = [0,0]

BODY_SGX = [0,0] 
BODY_SGY = [0,0]
BODY_SGXB = [0,0]
BODY_SGYB = [0,0]
BODY_SPEED_GX = [0,0]
BODY_SPEED_GY = [0,0]

# num_frames = 50

while cv2.waitKey(1) < 0:
    t = time.time()
    hasFrame, frame = cap.read()
    frameCopy = np.copy(frame)

    if not hasFrame:
        cv2.waitKey()
        break

    frameWidth = frame.shape[1]
    frameHeight = frame.shape[0]

    inpBlob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (inWidth, inHeight),
                              (0, 0, 0), swapRB=False, crop=False)
    net.setInput(inpBlob)
    output = net.forward()

    H = output.shape[2]
    W = output.shape[3]

    # Empty list to store the detected keypoints
    points = []
    GX = []
    GY = []

    # CSV
    def dataset():

        for i in range(nPoints):
            probMap = output[0, i, :, :]

            minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

            x = (frameWidth * point[0]) / W
            y = (frameHeight * point[1]) / H

            if prob > threshold : 
                cv2.circle(frameCopy, (int(x), int(y)), 8, (0, 255, 255), thickness=-1, lineType=cv2.FILLED)
                cv2.putText(frameCopy, "{}".format(i), (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, lineType=cv2.LINE_AA)

                points.append((int(x), int(y)))
                GX.append(int(x))
                GY.append(int(y))

            else :
                points.append(None)
                GX.append(1)
                GY.append(1)

        #DRAW SKELETON ABOUT HEAD
        for HEAD in POSE_HEAD:
            partA = HEAD[0]
            partB = HEAD[1]

            if points[partA] and points[partB] :
                cv2.line(frame, points[partA], points[partB], (0, 255, 255), 3, lineType=cv2.LINE_AA)
                cv2.circle(frame, points[partA], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)
                cv2.circle(frame, points[partB], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)
                
            if GX[partA] and GY[partA]:
                HEAD_SGX.insert(0,GX[partA])
                HEAD_SGY.insert(0,GY[partA])

            for x in HEAD_SGX :
                a1 = HEAD_SGX[0]
                a2 = HEAD_SGX[1]

                S = a2 - a1
                ReGanX = S / (1 + (time.time() - t))

            for x in HEAD_SGY :
                y1 = HEAD_SGY[0]
                y2 = HEAD_SGY[1]

                v = y2 - y1
                ReGanY = v / (1 + (time.time() - t))

            HEAD_SPEED_GY.insert(0,ReGanY)
            HEAD_SPEED_GX.insert(0,ReGanX)

            for x in HEAD_SPEED_GX :
                t1 = TIME_FRAME[0]
                t2 = TIME_FRAME[1]
                sp1 = HEAD_SPEED_GX[0]
                sp2 = HEAD_SPEED_GX[1]

                A = sp1 - sp2
                T = t1 - t2
                T = T + 1
                SPEEPD_HEAD = A / np.absolute(T) 
            
        head = [
                ["X1", GX[partA]],
                ["Y1", GY[partA]],
                ["SpeedX", ReGanX],
                ["SpeedY", ReGanY],
                ["Accelerate_Head", SPEEPD_HEAD]
            ]

        for SHOULDER in POSE_SHOULDER :
            partA = SHOULDER[0]
            partB = SHOULDER[1]

            if points[partA] and points[partB] :
                cv2.line(frame, points[partA], points[partB], (0, 255, 255), 3, lineType=cv2.LINE_AA)
                cv2.circle(frame, points[partA], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)
                cv2.circle(frame, points[partB], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)

            if GX[partA] and GY[partA] :
                SHOULDER_SGX.insert(0,GX[partA])
                SHOULDER_SGY.insert(0,GY[partA])

            for x in SHOULDER_SGX :
                a1 = SHOULDER_SGX[0]
                a2 = SHOULDER_SGX[1]

                S = a2 - a1
                ReGanX = S / (1 + (time.time() - t))

            for x in SHOULDER_SGY :
                y1 = SHOULDER_SGY[0]
                y2 = SHOULDER_SGY[1]

                v = y2 - y1
                ReGanY = v / (1 + (time.time() - t))

            SHOULDER_SPEED_GX.insert(0,ReGanX)
            SHOULDER_SPEED_GY.insert(0,ReGanY)

            for x in SHOULDER_SPEED_GX :
                t1 = TIME_FRAME[0]
                t2 = TIME_FRAME[1]
                sp1 = SHOULDER_SPEED_GX[0]
                sp2 = SHOULDER_SPEED_GX[1]

                A = sp1 - sp2
                T = t1 - t2
                T = T + 1
                SPEEPD_SHOULDER = A / np.absolute(T) 

        shoulder = [
                ["X2", GX[partA]],
                ["Y2", GY[partA]],
                ["SpeedX", ReGanX],
                ["SpeedY", ReGanY],
                ["Accelerate_Shoulder", SPEEPD_SHOULDER]
            ]

        # shoulder = [GX[partA],GY[partB],ReGanX,ReGanY,SPEEPD_SHOULDER]

        for BODY in POSE_BODY :
            partA = BODY[0]
            partB = BODY[1]

            if points[partA] and points[partB] :
                cv2.line(frame, points[partA], points[partB], (0, 255, 255), 3, lineType=cv2.LINE_AA)
                cv2.circle(frame, points[partA], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)
                cv2.circle(frame, points[partB], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)

            if GX[partA] and GY[partA] :
                BODY_SGX.insert(0,GX[partA])
                BODY_SGY.insert(0,GY[partA])
            
            if GX[partB] and GY[partB] :
                BODY_SGXB.insert(0,GX[partB])
                BODY_SGYB.insert(0,GY[partB])

            for x in BODY_SGX :
                a1 = BODY_SGX[0]
                a2 = BODY_SGX[1]

                S = a2 - a1
                ReGanX = S / (1 + (time.time() - t))

            for x in BODY_SGY :
                y1 = BODY_SGY[0]
                y2 = BODY_SGY[1]

                v = y2 - y1
                ReGanY = v / (1 + (time.time() - t))

            BODY_SPEED_GX.insert(0,ReGanX)
            BODY_SPEED_GY.insert(0,ReGanY)
            
            for x in BODY_SPEED_GX :
                t1 = TIME_FRAME[0]
                t2 = TIME_FRAME[1]
                sp1 = BODY_SPEED_GX[0]
                sp2 = BODY_SPEED_GX[1]

                A = sp1 - sp2
                T = t1 - t2
                T = T + 1
                SPEEPD_BODY = A / np.absolute(T) 
            
            # Find Angle
            point0 = [BODY_SGX[0], BODY_SGY[0]]
            point1 = [BODY_SGXB[0], BODY_SGYB[0]]
            point2 = [BODY_SGX[1], BODY_SGY[1]]
            point3 = [BODY_SGXB[1], BODY_SGYB[1]]

            global v1, v2, minusPoint, toList1, toList2, vector1, vector2, dotResult, length_v1, length_v2, cosZeta, resultArccos, convertToDegrees

            minusPoint = np.array(point0) - np.array(point2)
            
            if minusPoint[0] <= 0 or minusPoint[1] <= 0 :
                v1 = np.array(minusPoint) + np.array(point2)
                v2 = np.array(minusPoint) + np.array(point3)

                
            elif minusPoint[0] > 0 or minusPoint[1] > 0 :
                v1 = np.array(point2) + np.array(minusPoint)
                v2 = np.array(point3) + np.array(minusPoint)

            toList1 = v1.tolist()
            toList2 = v2.tolist()

            vector1 = point0 + point1
            vector2 = toList1 + toList2
            dotResult = np.dot(vector1, vector2)

            length_v1 = np.linalg.norm(vector1)
            length_v2 = np.linalg.norm(vector2)
            cosZeta = round(dotResult / (length_v1*length_v2), 15)
                        
            resultArccos = np.arccos(cosZeta)
            convertToDegrees = np.degrees(resultArccos)

        body = [
                ["X3", GX[partA]],
                ["Y3", GY[partA]],
                ["SpeedX", ReGanX],
                ["SpeedY", ReGanY],
                ["Accelerate_Body", SPEEPD_BODY],
                ["Angle", convertToDegrees]
            ]

        # body = [GX[partA],GY[partB],ReGanX,ReGanY,SPEEPD_BODY,convertToDegrees]

        TIME_FRAME.insert(0,((time.time() + 1) - t))        

        with open ('demo.csv', 'a', newline='', encoding='utf8') as f:
            file_is_empty = os.stat('demo.csv').st_size == 0
            fieldnames = ['Head','Shoulder','Body','Result']
            fw = csv.DictWriter(f, fieldnames=fieldnames)
            
            if file_is_empty:
                fw.writeheader()

            fw.writerow({'Head' : head, 'Shoulder' : shoulder, 'Body' : body})

    dataset()

    # end = time.time()
    # seconds = end - t
    # fps  = num_frames / seconds

    # cv2.putText(frame, "time taken = {:.2f} sec".format(time.time() - t), (50, 50), cv2.FONT_HERSHEY_COMPLEX, .8, (255, 50, 0), 2, lineType=cv2.LINE_AA)
    # cv2.putText(frame, "{:.2f} fps".format(fps), (50, 75), cv2.FONT_HERSHEY_COMPLEX, .8, (0, 50, 150), 2, lineType=cv2.LINE_AA)

    cv2.imshow('Output-Skeleton', frame)