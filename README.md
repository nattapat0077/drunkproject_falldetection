# DrunkProject_FallDetection ICT20

The project test on Ubuntu 16.04.1

1. Python3
2. Opencv3

## Installation

```sh
git clone https://nattapat0077@bitbucket.org/nattapat0077/drunkproject_falldetection.git
```

## Files

- FallDetectProgram.py
```python
#For Video Files
input_source = "Data/01.mp4" 
cap = cv2.VideoCapture(input_source)

#Camera webcam
cap = cv2.VideoCapture(0)
```

- editQuoteCSV.py for add **Quote** to CSV file
```python
#Read file
with open('dataset.csv', 'r') as csv_file:

#Write file
with open('dataset_model.csv', 'w') as new_file:
```

- For_Demo.py for demo and this file same FallDetectProgram.py

## Usage

- For Windows
```sh
python FallDetectProgram.py
```

- For Linux(Ubuntu)
```sh
python3 FallDetectProgram.py
```

### Edit Header CSV
See how to edit header Step-by-Step on *Youtube*

[![Edit header Step-by-Step](https://img.youtube.com/vi/hIyW1gYZgjU/0.jpg)](https://www.youtube.com/watch?v=hIyW1gYZgjU "Edit header Step-by-Step")

- Edit header dataset.csv on Excel
```
"[['X1', 0], ['Y1', 0], ['SpeedX', 0.0], ['SpeedY', 0.0], ['Accelerate_Head', 0.0]]","[['X2', 0], ['Y2', 0], ['SpeedX', 0.0], ['SpeedY', 0.0], ['Accelerate_Shoulder', 0.0]]","[['X3', 0], ['Y3', 0], ['SpeedX', 0.0], ['SpeedY', 0.0], ['Accelerate_Body', 0.0], ['Angle', 0.0]]",
```

- Done!! and save dataset_editHeader.csv
```
X1,Y1,SpeedX_Head,SpeedY_Head,Accelerate_Head,X2,Y2,SpeedX_Shoulder,SpeedY_Shoulder,Accelerate_Shoulder,X3,Y3,SpeedX_Body,SpeedY_Body,Accelerate_Body,Angle,Result
0,0,0,0,0,0,0,0,0,0.0,0,0,0,0,0,0,0
```

- Add Quote in dataset_editHeader.csv follwing command
```
python editQuoteCSV.py 
OR
python3 editQuoteCSV.py
```

- Result on file dataset_model

## Thank you
[Learn OpenCV](https://www.learnopencv.com/deep-learning-based-human-pose-estimation-using-opencv-cpp-python/)