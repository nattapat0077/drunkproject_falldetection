import csv

with open('dataset.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    with open('dataset_model.csv', 'w') as new_file:
        csv_writer = csv.writer(new_file, quoting=csv.QUOTE_ALL)

        for line in csv_reader:
            csv_writer.writerow(line)